<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class actor extends Model
{
    use HasFactory;
    public function movies()
    {
        return $this->belongsTo(movies::class);
    }
    protected $fillable = [
        'first_name',
        'last_name',
        'image'
    ];
}
