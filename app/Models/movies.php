<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class movies extends Model
{
    use HasFactory;
    public function actor()
    {
        return $this->belongsTo(actor::class);
    }

    protected $fillable = [
        'movie_title',
        'release_year',
        'director_name',
        'actor_id'
    ];
}
