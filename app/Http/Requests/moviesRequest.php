<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;


class moviesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'movie_title' => 'required|unique:movies|max:255',
            'release_year' => 'required|min:4|max:4',
            'director_name' => 'required|max:255',
            'actor_id' => 'required',
        ];
    }

    public function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json([
            'success'   => false,
            'message'   => 'Validation errors',
            'data'      => $validator->errors()
        ],404));
    }
    public function messages() //OPTIONAL
    {
        return [
            'movie_title.required' => 'Movie Title is required',
            'release_year.required' => 'Release Year is required',
            'director_name.required' => 'Director Name is required',
            'actor_id.required' => 'Actor ID is required',
        ];
    }
}
