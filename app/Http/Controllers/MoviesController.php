<?php

namespace App\Http\Controllers;

use App\Models\movies;
use Illuminate\Http\Request;
use App\Http\Requests\moviesRequest;
use App\Http\Resources\movieResource;


class MoviesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return movieResource::collection(movies::paginate(25));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(moviesRequest $request)
    {
        $movie = movies::create([
            'movie_title' => $request->movie_title,
            'release_year' => $request->release_year,
            'director_name' => $request->director_name,
            'actor_id' => $request->actor_id,
        ]);
        return response()->json([
            "success" => true,
            "message" => "Movie Is Created",
            'data' => new movieResource($movie)
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\movies  $movies
     * @return \Illuminate\Http\Response
     */
    public function show(movies $movies)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\movies  $movies
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, movies $movies)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\movies  $movies
     * @return \Illuminate\Http\Response
     */
    public function destroy(movies $movies)
    {
        //
    }
}
