<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class movieResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'movie_title' => $this->movie_title,
            'release_year' => $this->release_year,
            'director_name' => $this->director_name,
            'actor' => $this->actor,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
        ];
    }
}
