<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class actorFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->firstNameMale(),
            'last_name' => $this->faker->lastName(),
            'image' => $this->faker->imageUrl(),
        ];
    }
}
