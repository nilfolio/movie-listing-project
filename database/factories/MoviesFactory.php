<?php

namespace Database\Factories;

use App\Models\actor;

use Illuminate\Database\Eloquent\Factories\Factory;

class MoviesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        $moviFacker = \Faker\Factory::create();
        $moviFacker->addProvider(new \Xylis\FakerCinema\Provider\Movie($moviFacker));
        $faker = \Faker\Factory::create();
        $faker->addProvider(new \Xylis\FakerCinema\Provider\Person($faker));
        return [
            'movie_title' => $moviFacker->movie,
            'release_year' => $this->faker->year(),
            'director_name' => $faker->maleDirector,
            'actor_id'  => function () {
                return actor::factory()->create()->id;
            }
        ];
    }
}
